/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_mv_nil_to_empty.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbarbari <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/31 17:56:07 by mbarbari          #+#    #+#             */
/*   Updated: 2015/05/31 17:56:13 by mbarbari         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_sh.h>

void ft_mv_nil_to_empty(t_btree *tree)
{
	char	*empty[1];
	int		i;

	empty[0] = "\0";
	i = 0;
	if (!tree)
		return ;
	if (tree->left)
		ft_mv_nil_to_empty(tree->left);
	if (tree->args_tab == NULL)
		tree->args_tab = empty;
	if (tree->right)
		ft_mv_nil_to_empty(tree->right);
}
