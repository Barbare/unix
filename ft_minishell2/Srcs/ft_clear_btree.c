/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_clear_btree.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbarbari <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/31 17:53:12 by mbarbari          #+#    #+#             */
/*   Updated: 2015/05/31 17:53:14 by mbarbari         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_sh.h>

void ft_clear_btree(t_btree **tree)
{
	t_btree	*tmp;
	int		i;

	tmp = *tree;
	if (!tree)
		return ;
	if (tmp->left != NULL)
		ft_clear_btree(&tmp->left);
	if (tmp->right != NULL)
		ft_clear_btree(&tmp->right);
	i = 0;
	while (tmp->args_tab != NULL && tmp->args_tab[i])
	{
		ft_strdel(&tmp->args_tab[i]);
		i++;
	}
	if (tmp->args_tab)
	{
		free(tmp->args_tab);
		tmp->args_tab = NULL;
	}
	free(tmp);
	*tree = NULL;
}
