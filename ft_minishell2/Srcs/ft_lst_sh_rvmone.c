/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lst_sh_rvmone.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbarbari <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/31 17:55:26 by mbarbari          #+#    #+#             */
/*   Updated: 2015/05/31 17:55:47 by mbarbari         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_sh.h>

/*
** This function remove a element of a linked list.
** Be careful, as t_lst_dbin is a simple linked list, to use this function,
** you need to send the element before the one you want to pull out.
*/
t_lst_dbin ft_lst_sh_rvmone(t_lst_dbin *prev)
{
	t_lst_dbin *cpy;

	if (prev && prev->next)
	{
		if (prev->next->next == NULL)
		{
			cpy = prev->next;
			prev->next = NULL;
		}
		else
		{
			cpy = prev->next;
			prev->next = prev->next->next;
			cpy->next = NULL;
		}
	}
}
