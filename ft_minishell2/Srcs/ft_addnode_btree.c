/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_addnode_btree.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbarbari <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/31 17:52:57 by mbarbari          #+#    #+#             */
/*   Updated: 2015/05/31 17:52:59 by mbarbari         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_sh.h>

void ft_addnode_btree(t_btree **tree, t_operand op, t_btree *n)
{
	t_btree *tmp;

	if (tree && *tree)
	{
		tmp = *tree;
		if (op <= tmp->operand && op != o_pipe &&
				(op < o_red_dl || op > o_red_e))
			ft_addnode_btree(&(*tree)->right, op, n);
		else
			ft_addnode_btree(&(*tree)->left, op, n);
	}
	else
		*tree = n;
}
