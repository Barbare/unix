/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printrev_btree.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbarbari <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/29 17:57:40 by mbarbari          #+#    #+#             */
/*   Updated: 2015/05/29 18:02:06 by mbarbari         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_sh.h>

void	ft_printrev_btree(t_btree *tree)
{
	int		i;

	i = 0;
	if (!tree)
		return ;
	if (tree->right)
		ft_printrev_btree(tree->right);
	ft_putnbr_dl(tree->operand);
	if (tree->cde_name)
		ft_putendl(tree->cde_name);
	while (tree->args_tab && tree->args_tab[i])
	{
		ft_putendl(tree->args_tab[i]);
		i++;
	}
	if (tree->left)
		ft_printrev_btree(tree->left);
}
