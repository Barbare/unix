/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbarbari <mbarbari@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/08 14:12:32 by mbarbari          #+#    #+#             */
/*   Updated: 2015/09/30 10:58:15 by mbarbari         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_p.h>
#include <libft.h>
//#include <ft_checking.h>

static t_env ft_init_env(int port)
{
	t_env	env;

	ft_bzero(&env, sizeof(t_env));
	env = (t_env) {.done = 1, .port = port};
	return (env);
}

int		main(int argc, char **argv)
{
	t_env	env;

	if (argc != 2 && ft_atoi(argv[1]) != -1) // TODO: rajouter si arguement n'est pas un chiffre
		ft_printf("Il manque un port pour fonctionner du con\n");
		//N_INFO(argv[0], "[port]")
	env = ft_init_env( ft_atoi(argv[1]));
	init_server(&env, ft_atoi(argv[1]));
	ft_exec(&env);
	return (0);
}
