/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_init.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbarbari <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/09 02:24:03 by mbarbari          #+#    #+#             */
/*   Updated: 2015/09/30 12:03:10 by mbarbari         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_p.h>

void	init_server(t_env *env)
{
	struct protoent     *proto;

	proto = getprotobyname("tcp");
	TF_ERR(proto == 0, "Initialization of tcp connection failed\n");
	TF_ERR((env->sock = socket(PF_INET, SOCK_STREAM, proto->p_proto)) >= 0,
			"Create socket failed with error %d", env->sock);
	ft_printf("Inialization of tcp connection %sOK\n", C_GREEN);
	env->sin.sin_family = AF_INET;
	env->sin.sin_port = htons(env->port);
	env->sin.sin_addr.s_addr = htonl(INADDR_ANY);
}

int		new_server(t_env *env)
{
	TF_ERR(bind(env->sock,
				(const t_sockaddr *)&env->sin, sizeof(env->sin)) == 0,
			"Bind cannot initialized");
	TF_ERR(listen(env->sock, 2048) == 0,
			"Cannot listen into port network %d", env->port);
	env->fd_acc = accept(env->sock, (t_sockaddr *)&env->csin, env->cslen)
	ft_printf("Listening port activated !\nWaiting message ...\n");
	TF_ERR(!(env->fd_acc), "Cannot accept socket"); 
	return (env->done = TRUE);
}

int		close_server(t_env *env)
{
	ft_printf("Close server...");
	if (close(env->acc) == 0)
		return (RN_ERR("Can't close file descriptor of accept socket"),
				env->done);
	if (close(env->sock) == 0)
		return (RN_ERR("Can't close socket."), env-done);
	ft_printf("Server has been closed...");
	return ((env->done = FALSE));
}
