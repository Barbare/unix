/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_p.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mbarbari <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/05/09 00:21:46 by mbarbari          #+#    #+#             */
/*   Updated: 2015/09/30 12:03:21 by mbarbari         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_P_H
# define FT_P_H
# include <libft.h>
# include <netdb.h>
# include <unistd.h>
# include <ft_printf.h>
# include <arpa/inet.h>
# include <sys/socket.h>
# include <netinet/in.h>

typedef struct sockaddr t_s_sockaddr; 

typedef struct		s_env
{
	char				done	: 1;
	unsigned int		cslen;
	int					sock;
	int					acc;
	int					port;
	int					fd_acc;
	struct sockaddr_in	sin;
	struct sockaddr_in	csin;
}					t_env;

void		init_server(t_env *env);
int			new_server(t_env *env);
int			close_server(t_env *env);
void		ft_exec(t_env *env);

#endif
